const Ajv = require("ajv").default;
const ajv = new Ajv({ allErrors: true });
const addFormats = require("ajv-formats");
require("ajv-errors")(ajv);

addFormats(ajv, { formats: ["date", "email"], keywords: true });

ajv.addFormat("no-space", {
  validate: (string) => string.indexOf(" ") === -1,
});

const schema1 = {
  type: "object",
  properties: {
    data: {
      type: "string",
      format: "no-space",
      errorMessage: {
        format: "'data' should have no space",
      },
    },
  },
};

validateDataList({ data: "axela" }, "sampleSchema1", schema1);
validateDataList({ data: "axela " }, "sampleSchema2", schema1);

const schema2 = {
  type: "array",
  additionalProperties: false,
  items: {
    type: "object",
    required: ["userId", "username", "email", "birthdate"],
    properties: {
      userId: {
        type: "integer",
        minimum: 1,
        maximum: 5,
        errorMessage:
          '"userId" should be integer type with minimum 1 and maximum 5 digits in length',
      },
      username: {
        type: "string",
        maxLength: 10,
        errorMessage:
          '"username" should be string with maximum 10 character length',
      },
      email: {
        type: "string",
        maxLength: 256,
        format: "email",
        errorMessage: {
          maxLength: "must NOT have more than 256 characters",
        },
      },
      birthdate: {
        type: "string",
        format: "date",
      },
    },
    errorMessage: {
      required: {
        userId: '"userID" is required',
        username: '"username" is required',
        email: '"email" is required',
        birthdate: '"birthdate" is required',
      },
    },
  },
};

userSchema1 = [
  {
    userId: 1,
    username: "axela",
    email: "axela@gmail.com",
    birthdate: "2018-01-12",
  },
  {
    userId: 2,
    username: "krizcheev",
    email: "krizcheev@gmail.com",
    birthdate: "2018-02-13",
  },
];

userSchema2 = [
  {
    userId: 1,
    username: "axela",
    email: "axela@gmail.com",
    birthdate: "2018-01-12",
  },
  {
    userId: 2,
    username: "krizcheev",
    email: "krizcheev@gmail",
    birthdate: "2018-02-13",
  },
];

userSchema3 = [
  {
    userId: 1,
    username: "axela",
    email: "axela@gmail.com",
    birthdate: "2018-01-12",
  },
  {
    userId: 2,
    username: "krizcheev",
    email: "krizcheev@gmail.com",
  },
];

userSchema4 = [
  {
    userId: 6,
    username: "axela",
    email: "axela@gmail.com",
    birthdate: "2018-01-12",
  },
  {
    userId: 2,
    username: "krizcheev",
    email: "krizcheev@gmail.com",
    birthdate: "2018-02-13",
  },
];

userSchema5 = [
  {
    userId: 1,
    username: "axela",
    email: "axela@gmail.com",
    birthdate: "2018-01-12",
  },
  {
    userId: 2,
    username: "krizcheev123",
    email: "krizcheev@gmail.com",
    birthdate: "2018-02-13",
  },
];
validateDataList(userSchema1, "userSchema1", schema2);
validateDataList(userSchema2, "userSchema2", schema2);
validateDataList(userSchema3, "userSchema3", schema2);
validateDataList(userSchema4, "userSchema4", schema2);
validateDataList(userSchema5, "userSchema5", schema2);

function validateDataList(data, schemaName, schema) {
  const validate = ajv.compile(schema);
  validate(data);
  if (validate.errors !== null) console.log(schemaName, validate.errors);
  else console.log(schemaName, "is valid");
}
